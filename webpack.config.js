const path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports={
	entry: path.join(__dirname,'./src/main.js'),
	output:{
		path:path.join(__dirname,'./dist'),
		filename:'bundle.js'
	},
	module:{
		rules:[
			{test:/\.css$/,use:['style-loader','css-loader']},
			{test: /\.(jpg|png|bmp|gif|jpeg)$/, use: ['url-loader?limit=5000&name=[hash:8]-[name].[ext]']},
			{test: /\.(eot|svg|woff|woff2|ttf)$/, use: ['url-loader?limit=5000&name=[hash:8]-[name].[ext]']}, //处理字体文件的loader
			{test: /\.js$/, use: ['babel-loader'],exclude:/node_modules/},
			{test:/\.vue$/,use:'vue-loader'} //处理.vue文件的loader
		]
	},
	plugins: [
        new HtmlWebpackPlugin({
             template: './src/index.html',
             filename:'index.html'
        })     
	],
	// resolve:{
	// 	alias:{  //修改vue被导入时的包的路径
	// 		"vue$":"vue/dist/vue.js"
	// 	}
	// }
}
