// 导入vue
import Vue from 'vue'
//导入router.js
import router from './router.js'
// 导入app组件
import app from "./app.vue"



// 导入mui样式、字体
import './lib/mui.css'
import './lib/icons-extra.css'


// 按需导入mint-UI组件
import 'mint-ui/lib/style.css'
import {Header,Swipe,SwipeItem} from 'mint-ui';
Vue.component(Header.name, Header);
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);

// 导入vue-resource模块
import VueResource from 'vue-resource'
Vue.use(VueResource)

var vm = new Vue({
    el: '#app',
    data: {},
    methods: {},
    render: c => c(app),
    router,

})