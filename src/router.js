import Vue from 'vue'
import VueRouter from 'vue-router'
//手动安装VueRouter
Vue.use(VueRouter)

import home from './components/tabbar/home.vue'
import cart from './components/tabbar/cart.vue'
import member from './components/tabbar/member.vue'
import search from './components/tabbar/search.vue'

var router = new VueRouter({
    routes: [
        { path: '/', redirect: '/home' },
        { path: '/home', component: home },
        { path: '/cart', component: cart },
        { path: '/member', component: member },
        { path: '/search', component: search }

    ],
    linkActiveClass: 'mui-active'
})

export default router